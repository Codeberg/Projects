# Some Guidelines for successful projects on Codeberg


## Project flow

- Teams are established when there are no objections, or the Codeberg e.V. Presidium decides to ignore the objections.
- Anyone can ask to start a team.
- Advertizing a service for general use requires **two active maintainers**.
- Maintainers should be able to spend at least **two hours per month** for their time.
  If you cannot commit to doing so, please step back as a maintainer.
- Maintainers promise to remain available for onboarding new maintainers even after they stepped down.
  You are released from this obligation once a new maintainer successfully took over a project.


## Security and Privacy

Applies when dealing with user data, e.g. when working with containers.

- Please read the Privacy Policy of Codeberg and confirm that you will apply the rules.
- Ensure that **log files are preserved for at most 7 days**.
  - When you temporarily copy logs or excerpts for investigation,
    ensure to remove them after seven days, too.
    Do not keep old logs, for example in your home directory.
  - If you want to share logs from the system, **strip all personal information**:
    - Always strip IP addresses and usernames,
      unless they contain only information about you or your testing accounts.
    - Strip repo names and internal IDs unless the reference is public anyway.
      Example: A user publicly complains about a bug in their repository.
      The reference is already public and you can keep references in the repo.
      But: You find a bug on the server that affects a specific user or project.
      Do not share this reference.
    - Reduce the logs to a minimum.
- **Never copy data from our systems.** If you want to create a test environment,
  use our systems for this.
- Do not store **copies of data for more than 7 days**.
  You have this snapshot for testing in your home directly?
  For investigation you created a copy of some object?
  Ensure that it is removed when you log out of our system.
- Protect the access to our systems appropriately.
  SSH keys must be secured with a password/phrase.
  Never store passwords in plaintext on your systems.


## Working with Repositories

- When you start new repositories, make them [REUSE-compliant](https://reuse.software/).
  We appreciate when you work on making existing projects compliant.
- When accepting contributions, authorship must be kept in the resulting commits.
  This is especially relevant when applying suggestions from comments and when squashing pull requests.
- Try to behave in an unblocking way: Try to review pull requests as soon as possible. It helps motivate contributors.
  - If threads become stale, try to communicate the reason.
- Go for small incremental changes where possible.
  The larger your change, the higher the chance it will go stale and no one will benefit from your work in the end.
  It also helps others to pick up the next steps and reduces the danger of conflicting changes.
