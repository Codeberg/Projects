---
gitea: none
include_toc: true
---

# Contributing to Codeberg 🩵

Do you want to get involved in Codeberg?
Thank you so much!
Read on to learn how you can help.


## Getting Started

We recommend performing the following steps.
Each one is optional and can be skipped.

- Join our [Contributing to Codeberg room on Matrix](http://matrix.to/#/#contributing-to-codeberg:kle.li) ([Matrix](https://matrix.org) is a federated messaging protocol).
  Smalltalk is a good way to get to know the team and your potential next colleagues.
  Most communication within Codeberg happens on Matrix.
- Browse the open issues in this repository to get an idea of interesting projects.
  Comment in some if you would like to join,
  or take a deep dive into their resources and chats right away.
- Create a [new issue to introduce yourself](https://codeberg.org/Codeberg/Contributing/issues/new?template=ISSUE_TEMPLATE%2f01-Introduce-yourself.yaml).
  If there are blockers that prevent you from getting started,
  or if you have different interests than the existing team list, let us know!


### Finding interesting work

[Browse the open issues here](https://codeberg.org/Codeberg/Contributing/issues) and check if a team interests you.
Use the labels to filter the expectations.
Sometimes, we need someone to lead an effort and build a team.
Often, you can join an existing effort and collaborate with others already.

The **pinned issues**, at the top of the list, highlight some projects that are
either very active or forming right now.
Check them out in any case!

If something is unclear, don't be shy and **add a comment asking for more information**.

If you didn't find something that matches your interests,
please let us know if you have ideas on your own.
Codeberg's community maintenance allows you to work on things you are interested in.
Maybe it's a cool idea we didn't think about before.


### Inexperienced? Don't worry!

Many people feel like they cannot keep pace with an organization of the size of Codeberg.
But it's actually the opposite:
Codeberg is what it is due to the diverse nature of our contributors.
We are sure there is a place for you!

Codeberg does not only need Designers, User Experience experts,
frontend and backend developers, system administrators and communication managers.
We also need people who can help us connect the domains by bringing in new perspectives.

For some of our current core team members,
Codeberg was also a great learning opportunity.
It allowed them to dive into topics they were interested in,
and make rapid experiences therein.
If you want to learn,
please get in touch and we'll try to ensure you'll find a productive environment to extend your skills.


## Get help when you are stuck

Take a break from what you are doing and get your head free.
Then try to organize your thoughts, break your problem down,
and chat with others about the problem (for example in the Matrix room or in your issue).

If this doesn't help you, or you have dependencies (hardware, permissions etc)
or you can't figure out how to continue, you can usually ping one of these people:

- @fnetX [(Matrix)](https://matrix.to/#/@otto_richter:matrix.tu-berlin.de)
- @Gusted [(Matrix)](https://matrix.to/#/@gusted:matrix.org)

They can help you figure out where to continue.


## Start a new project

Something is missing? You want to help? Great!
If there is no issue yet, propose a new team in the issue tracker.
Explain that you want to take the lead.

To prevent projects from going stale once a person loses interest
or other circumstances prevent them from continuing their work,
we usually want to have at least two maintainers.
Please help us in finding a second person for your project by actively asking for help in the issue and chatrooms.


## Some of our projects

You can find more teams in the issue tracker.
This list adds a few information about the most relevant teams.


### Active Software Projects

#### [Forgejo](https://codeberg.org/forgejo/forgejo)

 * **Status:** Help always welcome
 * **Contact:** [#forgejo-development:matrix.org](https://matrix.to/#/#forgejo-development:matrix.org?via=mozilla.org?via=tchncs.de)
 * **Stack:** Go, CSS, JavaScript

#### [Codeberg Pages](https://codeberg.org/Codeberg/pages-server)

 * **Status:** Help welcomed!
 * **Contact:** [#gitea-pages-server:matrix.org](https://matrix.to/#/#gitea-pages-server:matrix.org?via=obermui.de?via=mozilla.org?via=tchncs.de)
 * **Stack:** Go
 * **Comment:** Looking for a challenge? Help us tackle stability issues!

#### [Registration Server](https://codeberg.org/Codeberg-e.V./registration-server)

 * **Status:** There's always room for improvement.
 * **Contact:** Issue Tracker, @momar, @f0sh
 * **Stack**: Go, HTML


### Community Projects

These projects do not necessarily require coding experience,
but benefit from other things like community management, writing or Public Relations Experience.

#### [Documentation](https://codeberg.org/Codeberg/Documentation/)

 * **Status:** Help welcome
 * **Contact:** Issue Tracker, [#codeberg-documentation:matrix.org at Matrix](https://matrix.to/#/#codeberg-documentation:matrix.org)
 * **Skills:** Writing, Spellchecking or JavaScript / Static Site Generators

#### [Community Issue Tracker Maintenance](https://codeberg.org/Codeberg/Community/issues/571)

 * **Status:** More help welcome
 * **Contact:** Issue Tracker or [Contributing to Codeberg Matrix Channel](http://matrix.to/#/#contributing-to-codeberg:kle.li)
 * **Skills:** Nothing special
 * **Comment:** Maintaining the public issue tracker and forwarding reports as appropriate

#### [Event Calendar](https://codeberg.codeberg.page/Events/)

 * **Status:** Needs more events
 * **Contact:** [Contributing to Codeberg Matrix Channel](http://matrix.to/#/#contributing-to-codeberg:kle.li)
 * **Skills:** Organizing
 * **Comment:** We are looking for people to organize regular (monthly / bi-weekly) meetings

#### [Matrix Spaces](https://matrix.to/#/#codeberg-space:matrix.org)

 * **Status:** OK
 * **Contact:** @n0toose, @gusted
 * **Skills:** Nothing special
 * **Comment:** Contact one of us you wish to add your room or space to the *Projects on Codeberg* Matrix space.

### Infrastructure Projects

Some of these require certain access levels to Codeberg's infrastructure.
If you are interested and commit a certain amount of time,
this can surely be talked about.
Please see the "Requirements" and "Comment" sections.

In case of upstream projects,
maintaining the service also includes collecting and forwarding bug reports to upstream development.

Many of the scripts that we use to host Codeberg can be found in a [specially designated organization](https://codeberg.org/Codeberg-Infrastructure).


#### Codeberg Pages

 * **Status:** Needs maintainers
 * **Contact:** [Repository](https://codeberg.org/Codeberg/pages-server)
 * **Requirements:** Some call to build trust

#### [Weblate](https://translate.codeberg.org)

 * **Status:** Co-maintainers welcome
 * **Contact:** Matrix: [#codeberg-translate:bubu1.eu](https://matrix.to/#/#codeberg-translate:bubu1.eu?via=matrix.org?via=mozilla.org?via=tchncs.de)
 * **Requirements:** Some call to build trust

#### [Continuous Integration](https://ci.codeberg.org)

 * **Status:** Co-maintainers very welcome
 * **Contact:** Matrix: [#codeberg-ci:obermui.de](https://matrix.to/#/#codeberg-ci:obermui.de?via=matrix.org?via=mozilla.org?via=tchncs.de)
 * **Requirements:** Some call to build trust, maybe some LXC/Docker experience

#### [Status Page / Monitoring](https://status.codeberg.org)

 * **Status:** Co-maintainers and innovations welcome
 * **Contact:** ?
 * **Requirements:** Some call to build trust

#### Forgejo ("Codeberg.org")

 * **Status:** OK, but contributions welcome
 * **Contact:** [Repository](https://codeberg.org/Codeberg-Infrastructure/build-deploy-forgejo/)
 * **Requirements:** Access to production machines limited to very trustworthy people
 * **Comment:** Suggestions (e.g. config improvements, new features etc) and some assistance when investigating issues welcome

#### Hardware, Ceph, MariaDB

 * **Status:** Experience welcome
 * **Contact:** [Skillsharing here](https://codeberg.org/Codeberg-Infrastructure/techstack-support)
 * **Requirements:** Access to production machines limited to very trustworthy people
 * **Comment:** Feel free to work with us on specific issues, and become a regular maintainer over time
